Marcin Jasiński 138936
Paweł Ostafin 150125

Użyte technologie:
PHP,  JSON

Schemat oraz opis architektury systemu:                                                                                                                                                                                  
Nasz projekt przedstawia mała wypożyczalnie książek która dzieli się na dwóch dostawców z czego jeden dostarcza książki napisane po polsku drugi natomiast po angielsku.                                        
	 
Opis dostawców, struktura encji:                                                                                                                                                                  
Provider 1 - książki w języku angielskim.                                                                                                                                                                                
Provider1 {	
id,
title,
authors,
description,
comments,
pages,
publisher}

Provider 2 - książki w języku polskim.                                                                                                                                                                                   
Provider2 {
id,
tytul,
autor,
opis,
komentarz,
strony,
wydawnictwo}

Opis huba: 
brak

Napotkane problemy: 
Utworzenie huba.

Adres do repozytorium:
https://jasinskim@bitbucket.org/jasinskim/ias.git

Podział pracy w grupie:                                                                                                                                                                                        
Marcin Jasiński - Provider 1, umieszczenie projektu na bitbucket.                                                                                                                                       
Paweł Ostafin - Provider 2, przygotowanie dokumentacji.

Co byśmy zmienili gdybyśmy robili ten projekt jeszcze raz?
Lepiej zaprojektowani providerzy.

